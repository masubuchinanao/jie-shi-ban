<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>投稿/コメント管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		body {
		    width: 1000px;
		    margin: auto;
		}
		h3{
		    width: 850px;
		}
		.kokuban{
			width: 850px;
		}
	</style>
	<script type="text/javascript">
		function check(){
			if(window.confirm('削除します。よろしいですか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
	</script>
</head>
<body>
<br>
	<h3><font size ="5">野菜の掲示板</font></h3>
		<div class="image"><br><br></div>
		<div class="header">
			<a href="./" class="btn-flat-border2">ホーム画面に戻る</a>
		</div>
		<div class="profile">
	        <h4>
	        	<font size="5" color="#a22041">
	        		<b><c:out value="${loginUser.name}" /></b>
	        	</font>さんでログイン中...
	        </h4>
	    </div>

		<div class="kokuban" >
			<span class="point"><b>投稿の検索</b></span><br>
			<form action="settingPost" method="get">
				<table>
					<tr>
						<td>
							▼<b>名前</b><br>
				   			<input type="text" name="name" value="${UserMessage.name}" placeholder="名前を検索"/>　　
				    	</td>
				    	<td>
							▼<b>カテゴリー</b><br>
				    		<input type="text" name="category" value="${UserMessage.category}" placeholder= "カテゴリーを検索" />　　
				    	</td>
				    	<td>
							▼<b>件名</b><br>
				    		<input type="text" name="subject" value="${UserMessage.subject}" placeholder="件名を検索"/>　　
				    	</td>
				    	<td>
							▼<b>本文</b><br>
				    		<input type="text" name="text" value="${UserMessage.text}" placeholder="本文を検索"/>　
				    	</td>
				    </tr>
				    <tr>
				    	<td>
							▼<b>日付<br>
				    		<input type= "date" name ="startdate" value ="${dates.startdate}"></input>から</b>
						</td>
						<td><br><b>
				    		<input type= "date" name ="enddate" value ="${dates.enddate}"></input>まで</b>
				    	</td>
				    	<td></td>
				    	<td><br>　　　　　<input type="submit" value="検索"  class="bt-samp1"></td>
				    </tr>
				</table>
			</form>
		</div><br>

		<h2>投稿一覧</h2><br>

		<table border="2" align="center">
			<tr bgcolor="#f19072">

				<th><font color="#fff">削除</font></th>
				<th><font color="#fff">名前</font></th>
				<th><font color="#fff">カテゴリー</font></th>
				<th><font color="#fff">件名</font></th>
				<th><font color="#fff">本文</font></th>
				<th><font color="#fff">投稿日時</font></th>

			</tr>
			<c:forEach items="${messages}" var="message">
				<tr bgcolor="#fce2c4">
					<td style="width:3em;">
						<form action="DeleteMessage" method="post" onSubmit="return check()">
							<input name="id" value="${message.id}" type="hidden"/>
							<b><input type="submit" name="deletemessage" value="削除" class="btn-flat-border20"></b>
						</form>
					</td>
					<td style="width:8em;"><font color="#d9333f"><c:out value="${message.user_name}" /></font></td>
					<td style="width:5em;"><font color="#d9333f"><c:out value="${message.category}" /></font></td>
					<td style="width:6em;"><font color="#d9333f"><c:out value="${message.subject}" /></font></td>
					<td style="width:20em;"><font color="#d9333f"><c:out value="${message.text}" /></font></td>
					<td style="width:6em;"><font color="#d9333f"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></font></td>
				</tr>
			</c:forEach>
		</table><br>

	<div class="image"><br><br></div>
	<div class="copyright"> Copyright(c)Nanao</div>
</body>
</html>