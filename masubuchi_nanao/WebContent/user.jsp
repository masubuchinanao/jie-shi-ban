<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>ユーザー管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function check(){
			if(window.confirm('変更します。よろしいですか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
	</script>
	<style type="text/css">
		.kokuban{
			width: 500px;
			margin-left: 320px
		}
		body{
			width: 1200px
		}
		.width{
			width: 850px;
			margin-right: auto;
			margin-left: auto;
		}
	</style>
</head>

	<body>
	<div class = "width">
	<h3><font size ="5">野菜の掲示板</font></h3>
		<div class="image"><br><br></div>
	 	<div class="header">
			<a href="newuser"class="btn-flat-border" >ユーザー新規登録</a>
			<a href="./" class="btn-flat-border2">ホーム画面に戻る</a>
		</div>
		<div class="profile">
	        <h4>
	        	<font size="5" color="#a22041">
	        		<b><c:out value="${loginUser.name}" /></b>
	        	</font>さんでログイン中...
	        </h4>
	    </div>

	    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                	<font size="4">　エラー</font>
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

        <div class="kokuban">
			<span class="point"><b>ユーザー検索</b></span><br>
			<form action="userlist" method="get">
				<table>
					<tr>
						<td>
							▼<b>名前</b><br>
				   			<input type="text" name="name" value="${UserMessage.name}" placeholder="名前を検索"/>
				    	</td>
				    	<td>
							▼<b>ログインID</b><br>
				    		<input type="text" name="login_id" value="${UserMessage.category}" placeholder= "ログインIDを検索" />
				    	</td>
				    	<td>
							▼<b>並び替え</b><br>
				    		<select name="sort">
				    			<option value="sort">選択してください</option>
								<option value="a">登録順</option>
								<option value="b">支店順</option>
								<option value="c">役職順</option>
								<option value="d">更新順</option>
							</select>
				    	</td>
				    </tr>
				    <tr>
				    	<td>
							▼<b>支店</b><br>
				    		<select name="branch_name">
				    			<option value="">選択してください</option>
					            <c:forEach items="${branch}" var="branch">
									<option value="${branch.name}">${branch.name}</option><br />
								</c:forEach>
							</select>
				    	</td>
				    	<td>
							▼<b>部署・役職</b><br>
							<select name="position_name">
								<option value="">選択してください</option>
					    		<c:forEach items="${position}" var="position">
									<option value="${position.name}">${position.name}</option>
								</c:forEach>
							</select>
				    	</td>
				    	<td><br>　　<input type="submit" value="検索" class="bt-samp1"></td>
				    </tr>
				</table>
			</form>
		</div>

		<h2>ユーザー情報一覧</h2>

	</div>
		<div class="userlist"><br>
		    <c:forEach items="${userlist}" var="user">
	            <div class="user">
					<table>
						<tr>
							<td>ログインID</td><td></td>
							<td><font color = "white"><c:out value="${user.login_id}" /></font></td>
						</tr>
						<tr>
							<td>名前</td><td></td>
							<td><font color = "white"><c:out value="${user.name}" /></font></td>
						</tr>
						<tr>
							<td>支店</td><td></td>
							<td><font color = "white"><c:out value="${user.branch_name}" /></font></td>
						</tr>
						<tr>
							<td>部署・役職</td><td></td>
							<td><font color = "white"><c:out value="${user.position_name}" /></font></td>
						</tr>
						<tr>
							<td><form action="setting" method="get">
									<input name="id" value="${user.id}" id="id" type="hidden"/>　
									<input type="submit" value="編集" class="bt-samp16">
								</form>
							</td>
							<td></td>
							<td>
								<c:if test="${user.id != loginUser.id}">
									<form method="post" action="account" onSubmit="return check()">
										<input name="id" value="${user.id}" id="id" type="hidden"/>
									<c:if test = "${user.is_working==1}">
										<input type="submit" value="停止" class ="bt-samp18">
											(<font color = "white">使用中</font>)
										<input name="working" value="${user.is_working}" type="hidden"/>
									</c:if>
									<c:if test = "${user.is_working==0}">
										<input type="submit" value="復活" class ="bt-samp18">
											(<b><font color = #ffff00>停止中</font></b>)
										<input name="working" value="${user.is_working}" type="hidden"/>
									</c:if>
									</form>
								</c:if>
							</td>
						</tr>
					</table>
	            </div>
		    </c:forEach>
		</div>
		<div class="image"><br><br></div>
		<div class="copyright"> Copyright(c)Nanao</div>
	</body>
</html>