
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<style>
		.kokuban {
		    color: #fff;
		    background-color: #114400;
		    margin: 10px 100px 10px 100px;
		    padding: 30px;
		    border: 9px solid #a60;
		    border-radius: 3px;
		    box-shadow: 2px 2px 4px #666, 2px 2px 2px #111 inset;
		    text-shadow: 0px 0px 2px #000;
		    line-height: 1.9;
		}
		</style>
    </head>
    <body>
    <div class="main-contents">
       <h3><font size ="5">野菜の掲示板</font><br></h3>
        <div class="image"><br><br></div>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                	<b><font size="4">　エラー</font></b> ：
                    <c:forEach items="${errorMessages}" var="message">
                        <c:out value="${message}" />
                    </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="login" method="post">
            <div class="kokuban">
					<span class="point"><font size="4">ログイン</font></span>
	            <div class ="form" align="center">
	                <label for="LoginId"><font color ="white">◇ログインID◇</font></label>　
	                <input type="text" name="login_id" id="LoginId" value = "${login.login_id }" placeholder="入力してください"/>
	                <font color ="white" size="4">*</font>
	                <br /><br /><br />
	                <label for="password"><font color ="white">◇パスワード◇</font></label>　
	                <input name="password" type="password" id="password" placeholder="入力してください"/>
	                <font color ="white" size="4">*</font>
	                <br /><br>
	                <input type="submit" value="ログイン" class="bt-samp1"/>
	                <br />
                </div>
            </div>
            </form><br>
            <div class="image"><br><br></div>
            <div class="copyright"> Copyright(c)Nanao</div>
            </div>
    </body>
</html>