<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        <h3><font size ="5">野菜の掲示板</font></h3>
        <div class="image"><br><br></div>

	 		<div class="header">
	 		<a href="./" class="btn-flat-border2">ホーム画面に戻る</a>
	        	<a href="userlist" class="btn-flat-border2">管理画面に戻る</a><br>
			</div>
			<div class="profile">
	        <h4>
	        	<font size="5" color="#a22041">
	        		<b><c:out value="${loginUser.name}" /></b>
	        	</font>さんでログイン中...
	        </h4>
	    	</div>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                	<font size="4">　エラー</font>
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            	<form action="newuser" method="post">
	                <input name="id" value="${newUser.id}"  type="hidden"/>
	                <div class="kokuban">
	                <span class="point">新規登録</span>
					<table>
						<tr>
							<td>
								◇名前◇
							</td>
							<td>
								<font size="2">(10文字まで)</font>
							</td>
							<td><input type="text" name="name" value="${newUser.name}" placeholder="入力してください"/>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇ログインID◇
							</td>
							<td>
								<font size="2">(半角英数／6～20文字)</font>
							</td>
							<td><input type="text" name="login_id" value="${newUser.login_id}" placeholder="入力してください"/>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇支店名◇
							</td>
							<td>

							</td>
							<td><select name="branch_name">
						            <c:forEach items="${branch}" var="branch">
										<option value="${branch.id}"
											<c:if test = "${branch.id == newUser.branch_id}">selected</c:if>>${branch.name}
										</option>
									</c:forEach>
								</select>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇部署・役職◇
							</td>
							<td>

							</td>
							<td><select name="position_name">
						            <c:forEach items="${position}" var="position">
										<option value="${position.id}"
											<c:if test = "${position.id == newUser.position_id}">selected</c:if>>${position.name}
										</option>
									</c:forEach>
								</select>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇パスワード◇
							</td>
							<td>
								<font size="2">(半角英数記号／6～20文字)</font>
							</td>
							<td><span class="focus_line"></span>
	                			<input name="password" type="password" id="password" placeholder="入力してください"/>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>

							</td>
							<td>
								<font size="2">※確認のため、もう一度入力してください</font>
							</td>
							<td><span class="focus_line"></span>
	                			<input name="password2" type="password" id="password" placeholder="入力してください"/>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr>
					</table>

					<div align ="right">
	            		<input type="submit" value="登録する" class="bt-samp1">　　　　　　　　
	            	</div>
					</div>

            	</form>

            <div class="image"><br><br></div>
            <div class="copyright">Copyright(c)Nanao</div>
        </div>
    </body>
</html>