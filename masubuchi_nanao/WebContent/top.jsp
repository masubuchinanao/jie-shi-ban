<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
        <script type="text/javascript">
			function check(){
				if(window.confirm('削除します。よろしいですか？')){ // 確認ダイアログを表示
					return true; // 「OK」時は送信を実行
				}else{ // 「キャンセル」時の処理
					window.alert('キャンセルされました'); // 警告ダイアログを表示
					return false; // 送信を中止
				}
			}
		</script>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
				.kokuban {
				    font-family: HuiFontP109;
				    color: #fff;
				    background-color: #114400;
				    margin: 10px 0 10px 420px;
				    padding: 30px;
				    border: 9px solid #a60;
				    border-radius: 3px;
				    box-shadow: 2px 2px 4px #666, 2px 2px 2px #111 inset;
				    text-shadow: 0px 0px 2px #000;
				    line-height: 1.9;
				}
		</style>
    </head>

    <body>
			<h3><font size ="5">野菜の掲示板</font></h3>
			<div class="image"><br><br></div>
			<div class="header">
				<c:if test="${loginUser.position_id == 2}">
					<a href="userlist" class="btn-flat-border">ユーザー管理</a>
				</c:if>
				<c:if test="${loginUser.position_id == 3}">
					<a href="settingPost" class="btn-flat-border">投稿/コメント管理</a>
				</c:if>
				<a href="newMessage" class="btn-flat-border">　新規投稿　</a>
				<a href="logout" class="btn-flat-border2">ログアウト</a>
			</div>
		    <div class="profile">
		        <h4>　こんにちは、
		        	<font size="5" color="#a22041">
		        		<b><c:out value="${loginUser.name}" /></b>
		        	</font>さん。
		        </h4>
		    </div>

	        <c:if test="${ not empty errorMessages }">
	            <div class="errorMessages">
	            <font size="5"><b>　エラー</b></font>
	                <ul>
	                    <c:forEach items="${errorMessages}" var="message">
	                        <li><c:out value="${message}" />
	                    </c:forEach>
	                </ul>
	            </div>
	            <c:remove var="errorMessages" scope="session"/>
	        </c:if>


			<div class="messages">

				<div class="kokuban">
					<span class="point"><b>投稿の検索</b></span><br>
					<form action="index.jsp" method="get">
					    ▼<b>日付　　　　　　　　　　　　　　　　　　　<br>
						    <input type= "date" name ="startdate" value ="${dates.startdate}"></input>から
						    <input type= "date" name ="enddate" value ="${dates.enddate}"></input>まで</b><br>
						▼<b>カテゴリー</b>　　　　　　　　　　　　　　<br>
						    <input type="text" name="category" value="${dates.category}" placeholder="カテゴリーを入力"/>　　　
						    <input type="submit" value="検索"  class="bt-samp1"><br>
					</form>
				</div>


			    <c:forEach items="${messages}" var="message">
					<div class="image"><br><br></div>
					<div class ="message">
						<div class="user_name">　
							<b><font size = "5" color = "#ff3c3c" ><c:out value="${message.user_name}" /></font></b>さんの投稿
						</div>



						<div class="category">　　　カテゴリー：
							<font color = "#ff3c3c" size="4">
								<c:out value="${message.category}" />
							</font>　 ／　　件名：
							<font color = "#ff3c3c" size="4">
								<c:out value="${message.subject}" />
							</font>
						</div>
						<div class="balloon1">
							<div class="text">
								<font color = "white" size="4">
									<c:forEach var="text" items="${fn:split(message.text, '
										')}" >
			    						<div>
			    							${text}
			    						</div>
									</c:forEach>
								</font>
							</div>
						</div>
						<div class="date">
							<font size="2" face="small-caption">
								　　　(<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />)
								<c:out value="${message.day}"></c:out>日と
								<c:out value="${message.hour}"></c:out>時間前の投稿
							</font>
						</div>
						<form action="DeleteMessage" method="post" onSubmit="return check()">
							<input name="id" value="${message.id}" type="hidden"/>
							<c:if test="${loginUser.id==message.user_id}">
								<b><input type="submit" name="deletemessage" value="この投稿を削除" class="btn-flat-border20"></b>
							</c:if>
						</form>
					</div>

					<c:forEach items="${comments}" var="comment">
					    <div class="comments">
						    <c:if test="${comment.message_id==message.id}">
								<div class="comment">
									<div class="user_name"align="right"><font color = "#ff8000" size="5">
										<b><c:out value="${comment.user_name}" /></b></font>さんのコメント　
									</div>


									<div class="balloon2">
										<span class="text">
											<font color = "white">
												<c:forEach var="s" items="${fn:split(comment.text, '
													')}">
						    						<div>${s}</div>
					    						</c:forEach>
											</font>
										</span>
									</div>
									<div class="date"align="right">
										<font size="2" face="small-caption">
											(<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />)　　　
										</font>
									</div>
									<div class="delete"align="right">
										<form action="DeleteComment" method="post" onSubmit="return check()">
											<input name="commentid" value="${comment.id}" id="id" type="hidden"/>
											<c:if test="${(loginUser.id==comment.user_id)}">
												<input type="submit" name="deletecomment" value="このコメントを削除"class="btn-flat-border20">　　
											</c:if>
										</form>
									</div>
								</div>
							</c:if>
						</div>
					</c:forEach>

					<div class="newComment">
						<c:if test="${ newComment }">
							<form action="newComment" method="post" >
								<div class="textarea" align="right"><br>
									<textarea name="comment"  class="comment-box" placeholder="コメントする　(500文字まで)"></textarea>　　
									<div align ="right">
										<input type="hidden" name="id" value="${message.id}">
										<input type="submit" value="コメント" class="bt-samp17">　　<br>
									</div><br>
								</div>

							</form>
						</c:if>
					</div>
				</c:forEach>
			</div>　　　<font size ="5" color ="#765c47">■<b>投稿は以上です</b>■</font>
			<br><hr size="3" color="green">
			<div class="image"><br><br></div>
			<div class="copyright"> Copyright(c)Nanao</div>
    </body>

</html>