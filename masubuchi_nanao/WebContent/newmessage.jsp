<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿画面</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

	<body>
	<h3><font size ="5">野菜の掲示板</font></h3>
	<div class="image"><br><br></div>

	 	<div class="header">
			<a href="./" class="btn-flat-border2">ホーム画面に戻る</a>
		</div><br>


			<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
	                <font size="4">　エラー</font>
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <c:remove var="errorMessages" scope="session" />

		<div class="form-area">
            <div class ="form">
		        <form action="newMessage" method="post">
		        	<input name="id" value="${newmessage.id}"  type="hidden"/>
		        	<div class="kokuban">
					<span class="point"><font size="4"><c:out value="${loginUser.name}" /></font>さんの投稿</span><br>
		        	<table>
						<tr>
							<td>
								◇カテゴリー◇<font size="2">(10文字まで)</font>
							</td>
							<td><input type="text" name="category" value="${editMessage.category}" placeholder="入力してください">
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇件名◇<font size="2">(30文字まで)</font>
							</td>
							<td><input type="text" name="subject" value="${editMessage.subject}" placeholder="入力してください">
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr><tr><td>　</td></tr>
						<tr>
							<td>
								◇本文◇<font size="2">(1000文字まで)</font>
							</td>
							<td><textarea name="text" cols="100" rows="15" placeholder="入力してください" >${editMessage.text}</textarea>
		            			<font color ="black" size="4">*</font>
		            		</td>
						</tr>
					</table>

		            	<div align ="right">
		            		<input type="submit" value="投稿する" class="bt-samp1">　　　　　　　　
		            	</div>
		            </div>
				</form>
			</div>
		</div>
		<div class="image"><br><br></div>
		<div class="copyright"> Copyright(c)Nanao</div>
	</body>
</html>