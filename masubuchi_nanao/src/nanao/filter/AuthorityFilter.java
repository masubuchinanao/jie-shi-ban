package nanao.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nanao.beans.User;

@WebFilter(urlPatterns= {"/userlist", "/newuser", "/setting"} , filterName="AuthorityFilter")
public class AuthorityFilter implements Filter{
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain){

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        try{
	        HttpSession session = req.getSession();
	        User user = (User) session.getAttribute("loginUser");

	        int position_id = user.getPosition_id();

	        if (!(position_id == 2 )) {
	        	String messages = "管理者権限がありません";
	        	session.setAttribute("errorMessages", messages);
	            res.sendRedirect("./");
	        	return;
	        }
	        chain.doFilter(request, response);
		    }catch (ServletException se){
		    }catch (IOException e){

		    }
        }
    public void init(FilterConfig filterConfig) throws ServletException{
    }
    public void destroy(){
    }
}