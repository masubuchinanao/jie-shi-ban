package nanao.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nanao.beans.User;

@WebFilter(urlPatterns="/*", filterName="LoginFilter")
public class LoginFilter implements Filter{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException{


	        HttpServletRequest req = (HttpServletRequest)request;
	        HttpServletResponse res = (HttpServletResponse)response;

	        String servletPath = req.getServletPath();

	        HttpSession session = req.getSession();
	        User user = (User) session.getAttribute("loginUser");

	        if (user == null && !servletPath.equals("/login") && !servletPath.equals("/css/style.css")) {
	        	String loginmessages = "ログインしてください";
	        	session.setAttribute("errorMessages", loginmessages);
	        	res.sendRedirect("login");
	        	return;
	        }
	        chain.doFilter(request, response);
	}
	public void init(FilterConfig filterConfig) throws ServletException{

	}
	public void destroy(){

	}
}
