package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.branches;
import nanao.exception.SQLRuntimeException;

public class BranchDao {

	public List<branches> getBranch(Connection connection) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM branches");

	        ps = connection.prepareStatement(sql.toString());

	        ResultSet rs = ps.executeQuery();
	        List<branches> branchList = toBranchList(rs);

	            return branchList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<branches> toBranchList(ResultSet rs) throws SQLException {

		List<branches> branch = new ArrayList<branches>();
		try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String branch_name = rs.getString("name");

                branches branches = new branches();
                branches.setId(id);
                branches.setName(branch_name);

                branch.add(branches);
            }
            return branch;
        } finally {
            close(rs);
        }
	}
}
