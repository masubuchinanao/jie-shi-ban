package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import nanao.exception.NoRowsUpdatedRuntimeException;
import nanao.exception.SQLRuntimeException;

public class DeleteMessageDao {


	public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM messages WHERE id = ?;";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            int Id = ps.executeUpdate();

            if (Id == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
            close(ps);
        }
	}
}
