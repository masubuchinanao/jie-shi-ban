package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.User;
import nanao.exception.NoRowsUpdatedRuntimeException;
import nanao.exception.SQLRuntimeException;

public class SettingDao {

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int isworking = rs.getInt("is_working");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_working(isworking);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", is_working = 1");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            if(user.getPassword().isEmpty() == false) {
            	sql.append(", password = ?");
            }
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranch_id());
            ps.setInt(4, user.getPosition_id());
            if(user.getPassword().isEmpty() == false) {
	            ps.setString(5, user.getPassword());
	            ps.setInt(6, user.getId());
            }else {
	            ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
