package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.positions;
import nanao.exception.SQLRuntimeException;


public class PositionDao {

	public List<positions> getPosition(Connection connection) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM positions");

	        ps = connection.prepareStatement(sql.toString());

	        ResultSet rs = ps.executeQuery();
	        List<positions> positionList = toPositionList(rs);

	            return positionList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<positions> toPositionList(ResultSet rs) throws SQLException {

		List<positions> position = new ArrayList<positions>();
		try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String branch_name = rs.getString("name");

                positions positions = new positions();
                positions.setId(id);
                positions.setName(branch_name);

                position.add(positions);
            }
            return position;
        } finally {
            close(rs);
        }
	}
}
