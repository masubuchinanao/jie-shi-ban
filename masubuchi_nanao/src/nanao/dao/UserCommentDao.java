package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.UserComment;
import nanao.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("users.name as user_name, ");
            sql.append("users.id as users_id ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY comments.created_date");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                int message_id = rs.getInt("message_id");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String user_name = rs.getString("user_name");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setMessage_id(message_id);
                comment.setText(text);
                comment.setCreated_date(createdDate);
                comment.setUser_name(user_name);

                ret.add(comment);

            }
            return ret;
        } finally {
            close(rs);
        }
    }
}