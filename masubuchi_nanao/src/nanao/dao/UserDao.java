package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.User;
import nanao.exception.SQLRuntimeException;


public class UserDao {

    public void insert(Connection connection, User users) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_working");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", 1"); // is_working
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, users.getLogin_id());
            ps.setString(2, users.getPassword());
            ps.setString(3, users.getName());
            ps.setInt(4, users.getBranch_id());
            ps.setInt(5, users.getPosition_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (3 <= userList.size()) {
                throw new IllegalStateException("3 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int isworking = rs.getInt("is_working");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User users = new User();
                users.setId(id);
                users.setLogin_id(login_id);
                users.setPassword(password);
                users.setName(name);
                users.setBranch_id(branch_id);
                users.setPosition_id(position_id);
                users.setIs_working(isworking);
                users.setCreatedDate(createdDate);
                users.setUpdatedDate(updatedDate);

                if(isworking == 1) {
                ret.add(users);
                }
            }
            return ret;
        } finally {
            close(rs);
        }
    }

//    public User getUser(Connection connection, int id) {
//
//        PreparedStatement ps = null;
//        try {
//            String sql = "SELECT * FROM users WHERE id = ?";
//
//            ps = connection.prepareStatement(sql);
//            ps.setInt(1, id);
//
//            ResultSet rs = ps.executeQuery();
//            List<User> userList = toUserList(rs);
//            if (userList.isEmpty() == true) {
//                return null;
//            } else if (2 <= userList.size()) {
//                throw new IllegalStateException("2 <= userList.size()");
//            } else {
//                return userList.get(0);
//            }
//        } catch (SQLException e) {
//            throw new SQLRuntimeException(e);
//        } finally {
//            close(ps);
//        }
//    }

	public List<User> getUser(Connection connection, String login_id) {

	    PreparedStatement ps = null;
	    try {

	        String sql = "SELECT * FROM users WHERE login_id = ?";

	        ps = connection.prepareStatement(sql);

	        ps.setString(1, login_id);

	        ResultSet rs = ps.executeQuery();
	        List<User> loginidList = tologinidList(rs);

	        if (loginidList.isEmpty() == true) {
	            return null;
	        } else if (2 <= loginidList.size()) {
	            throw new IllegalStateException("2 <= loginidList.size()");
	        } else {
	            return loginidList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}


	private List<User> tologinidList(ResultSet rs) throws SQLException{
        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int isworking = rs.getInt("is_working");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_working(isworking);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
	}
}