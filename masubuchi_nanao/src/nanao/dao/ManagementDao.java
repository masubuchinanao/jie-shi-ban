package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import nanao.beans.User;
import nanao.exception.SQLRuntimeException;

public class ManagementDao {

    public List<User> getUser(Connection connection, User user) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" users.id as id");
            sql.append(", users.login_id as login_id");
            sql.append(", users.password as password");
            sql.append(", users.name as name");
            sql.append(", branches.name as branch_name");
            sql.append(", branches.id as branch_id");
            sql.append(", positions.name as position_name ");
            sql.append(", positions.id as position_id ");
            sql.append(", users.is_working as is_working");
            sql.append(", users.updated_date as updated_date");
            sql.append(" FROM branches");
            sql.append(" INNER JOIN (users INNER JOIN positions ON users.position_id = positions.id) ");
            sql.append(" ON branches.id = users.branch_id ");
            sql.append("WHERE users.name like ?");
            sql.append("AND login_id like ? ");
            sql.append("AND positions.name like ? ");
            sql.append("AND branches.name like ? ");

            if(user.getSort() == null){
            	sql.append("ORDER BY id");
            }else if(user.getSort().equals("a")) {
            	sql.append("ORDER BY id");
            }else if(user.getSort().equals("b")) {
            	sql.append("ORDER BY branch_id desc");
            }else if(user.getSort().equals("c")) {
            	sql.append("ORDER BY position_id");
            }else if(user.getSort().equals("d")) {
            	sql.append("ORDER BY updated_date desc");
            }

            ps = connection.prepareStatement(sql.toString());

            if(StringUtils.isBlank(user.getName())) {
	        	ps.setString(1, "%");
	        }else {
        		ps.setString(1, "%" + user.getName() + "%");
        	}
	        if(StringUtils.isBlank(user.getLogin_id())) {
	        	ps.setString(2, "%");
	        }else {
	        	ps.setString(2, "%" + user.getLogin_id() + "%");
        	}
	        if(StringUtils.isBlank(user.getPosition_name())) {
	        	ps.setString(3, "%");
	        }else {
	        	ps.setString(3, "%" +user.getPosition_name()+ "%");
	        }
	        if(StringUtils.isBlank(user.getBranch_name())) {
	        	ps.setString(4, "%");
	        }else {
	        	ps.setString(4, user.getBranch_name());
	        }

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String position_name = rs.getString("position_name");
                int is_working = rs.getInt("is_working");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_name(branch_name);
                user.setPosition_name(position_name);
                user.setIs_working(is_working);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}