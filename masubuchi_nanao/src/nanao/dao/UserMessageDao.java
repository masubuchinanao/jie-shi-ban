package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import nanao.beans.UserMessage;
import nanao.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, String starttime, String endtime, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append(" messages.id as message_id ");
            sql.append(", messages.user_id as user_id ");
            sql.append(", messages.subject as subject ");
            sql.append(", messages.text as text ");
            sql.append(", messages.category as category ");
            sql.append(", messages.created_date as created_date ");
            sql.append(", users.id as users_id ");
            sql.append(", users.name as name ");
            sql.append("FROM messages ");
            sql.append("inner join users on messages.user_id = users.id ");
            sql.append("WHERE messages.created_date>=? AND messages.created_date<=? ");
            if(!(StringUtils.isEmpty(category))) {
            	sql.append("AND category like ?");
            }
            sql.append("ORDER BY created_date DESC");

	        ps = connection.prepareStatement(sql.toString());

	        if(!(StringUtils.isEmpty(category))) {
		        ps.setString(1, starttime);
		        ps.setString(2, endtime);
	        	ps.setString(3, "%" + category + "%");
	        }else {
	        	ps.setString(1, starttime);
		        ps.setString(2, endtime);
	        }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);



            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public List<UserMessage> getUserMessages(Connection connection, String starttime, String endtime, UserMessage UserMessage) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append(" messages.id as message_id ");
            sql.append(", messages.user_id as user_id ");
            sql.append(", messages.subject as subject ");
            sql.append(", messages.text as text ");
            sql.append(", messages.category as category ");
            sql.append(", messages.created_date as created_date ");
            sql.append(", users.id as users_id ");
            sql.append(", users.name as name ");
            sql.append("FROM messages ");
            sql.append("inner join users on messages.user_id = users.id ");
            sql.append("WHERE messages.created_date>=? AND messages.created_date<=? ");
            sql.append("AND category like ? ");
            sql.append("AND subject like ? ");
            sql.append("AND text like ? ");
            sql.append("AND name like ? ");
            sql.append("ORDER BY created_date DESC");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setString(1, starttime);
	        ps.setString(2, endtime);
	        if(StringUtils.isBlank(UserMessage.getCategory())) {
	        	ps.setString(3, "%");
	        }else {
        		ps.setString(3, "%" + UserMessage.getCategory() + "%");
        	}
	        if(StringUtils.isBlank(UserMessage.getSubject())) {
	        	ps.setString(4, "%");
	        }else {
	        	ps.setString(4, "%" + UserMessage.getSubject() + "%");
        	}
	        if(StringUtils.isBlank(UserMessage.getText())) {
	        	ps.setString(5, "%");
	        }else {
	        	ps.setString(5, "%" + UserMessage.getText() + "%");
        	}
	        if(StringUtils.isBlank(UserMessage.getUser_name())) {
	        	ps.setString(6, "%");
	        }else {
	        	ps.setString(6, "%" + UserMessage.getUser_name() + "%");
        	}

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                int message_id = rs.getInt("message_id");
                int user_id = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int users_id = rs.getInt("users_id");
                String name = rs.getString("name");

                UserMessage message = new UserMessage();
                message.setId(message_id);
                message.setUser_id(user_id);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreated_date(createdDate);
                message.setUsers_id(users_id);
                message.setUser_name(name);

                Date date = new Date();
                Date dateFrom = createdDate;

                // 日付をlong値に変換します。
                long dateTimeTo = date.getTime();
                long dateTimeFrom = dateFrom.getTime();

                // 差分の時間を算出します。
                long dayDiff = ( dateTimeTo - dateTimeFrom  ) / (1000 * 60 * 60);

                if(dayDiff < 24) {
                	long hour = dayDiff;
                	message.setHour(hour);
                }else {
                	long day = dayDiff / 24 ;
                	message.setDay(day);
                	long hour = dayDiff % 24 ;
                	message.setHour(hour);
                }

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
