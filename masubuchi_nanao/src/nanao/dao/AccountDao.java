package nanao.dao;

import static nanao.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import nanao.beans.User;
import nanao.exception.NoRowsUpdatedRuntimeException;
import nanao.exception.SQLRuntimeException;

public class AccountDao {

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int isworking = rs.getInt("is_working");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_working(isworking);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public User getUser(Connection connection, int id, int working) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        }else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        }else{
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void update(Connection connection, int id , int working) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET is_working = ?, updated_date = CURRENT_TIMESTAMP WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            if(working==1) {
                ps.setInt(1, 0);
                ps.setInt(2, id);
            }else {
	            ps.setInt(1, 1);
	            ps.setInt(2, id);
            }
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
