package nanao.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nanao.beans.User;
import nanao.beans.branches;
import nanao.beans.positions;
import nanao.service.BranchService;
import nanao.service.ManagementService;
import nanao.service.PositionService;

@WebServlet(urlPatterns = { "/userlist" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<branches> branches = new BranchService().getBranch();
    	request.setAttribute("branch", branches);
    	List<positions> positions = new PositionService().getPosition();
    	request.setAttribute("position", positions);

    	User user = new User();
    	user.setBranch_name(request.getParameter("branch_name"));
    	user.setPosition_name(request.getParameter("position_name"));
    	user.setLogin_id(request.getParameter("login_id"));
    	user.setName(request.getParameter("name"));
    	user.setSort(request.getParameter("sort"));

    	System.out.println("sort: "+user.getSort());

    	List<User> users = new ManagementService().getUser(user);

        request.setAttribute("userlist", users);

        request.getRequestDispatcher("user.jsp").forward(request, response);
    }
}