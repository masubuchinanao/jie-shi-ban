package nanao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nanao.beans.User;
import nanao.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String LoginId = request.getParameter("login_id");
        String Password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User users = loginService.login(LoginId, Password);

        HttpSession session = request.getSession();

        if (users != null) {

            session.setAttribute("loginUser", users);
            response.sendRedirect("./");

        } else {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインできません");

            User login = new User();
            login.setLogin_id(LoginId);
            login.setPassword(Password);

            request.setAttribute("login", login);

            session.setAttribute("errorMessages", messages);

            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
