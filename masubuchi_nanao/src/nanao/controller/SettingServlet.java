package nanao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import nanao.beans.User;
import nanao.beans.branches;
import nanao.beans.positions;
import nanao.exception.NoRowsUpdatedRuntimeException;
import nanao.service.BranchService;
import nanao.service.PositionService;
import nanao.service.SettingService;
import nanao.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();
    	HttpSession session = request.getSession();

	    try {
    		int id =Integer.parseInt(request.getParameter("id"));
    	    User editUser = new SettingService().getUser(id);


    	    String[] parameter = request.getParameterValues("id");

    	    if(editUser != null) {
		        request.setAttribute("editUser", editUser);

		    	List<branches> branches = new BranchService().getBranch();
		    	request.setAttribute("branch", branches);

		    	List<positions> positions = new PositionService().getPosition();
		    	request.setAttribute("position", positions);

		        request.getRequestDispatcher("setting.jsp").forward(request, response);
    	    }else if(parameter == null) {
    	    	messages.add("不正なアクセスです");
                session.setAttribute("errorMessages", messages);
	        	response.sendRedirect("userlist");
	        }else {
	        	messages.add("不正なアクセスです");
                session.setAttribute("errorMessages", messages);
	        	response.sendRedirect("userlist");
	        }
	    }catch(NumberFormatException e) {
	    	messages.add("不正なアクセスです");
            session.setAttribute("errorMessages", messages);
	    	response.sendRedirect("userlist");
	    }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);
        User users = (User) request.getSession().getAttribute("loginUser");
        int loginuserid = users.getId();

        if (isValid(request, messages) == true) {

            String password = request.getParameter("password");

            try {
                new SettingService().update(editUser, password);

            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("userlist").forward(request, response);
                return;
            }

            if(loginuserid == editUser.getId()) {
                session.setAttribute("loginUser", editUser);
            }
            session.setAttribute("editUser", editUser);

            response.sendRedirect("userlist");

        } else {

        	String name = request.getParameter("settingname");
    		request.setAttribute("settingname", name);

        	List<branches> branches = new BranchService().getBranch();
        	request.setAttribute("branch", branches);

        	List<positions> positions = new PositionService().getPosition();
        	request.setAttribute("position", positions);

            session.setAttribute("errorMessages", messages);

            request.setAttribute("editUser", editUser);

            request.getRequestDispatcher("setting.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_name")));
        editUser.setPosition_id(Integer.parseInt(request.getParameter("position_name")));
        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	int id =Integer.parseInt(request.getParameter("id"));
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String branch_name = request.getParameter("branch_name");

        int branch_id = Integer.parseInt(request.getParameter("branch_name"));
        String position_name = request.getParameter("position_name");
        int position_id = Integer.parseInt(request.getParameter("position_name"));

        List<User> loginidList = new UserService().getLoginId(login_id);

        System.out.println("setting id: "+ id);

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if(!(StringUtils.isBlank(name) == true) & name.length() > 10) {
        	messages.add("「名前」は10文字以内で入力してください");
        }
        if(loginidList != null) {
        	if(id != loginidList.get(0).getId()) {
        	messages.add("すでに使われているログインIDです");
        	}
        }
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (!(StringUtils.isBlank(login_id)) & !(login_id.matches("^[0-9a-zA-Z]{6,20}+$"))) {
        	messages.add("ログインIDは半角英数でかつ6文字以上20文字以下に設定してください");
        }
        if(branch_id == 17 && position_id > 3){
        	messages.add("本社の「部署・役職」を選択してください");
        }
        if(branch_id !=17 & position_id <= 3) {
        	messages.add("支店の「部署・役職」を選択してください");
        }
        if (StringUtils.isBlank(branch_name) == true) {
            messages.add("支店名を入力してください");
        }
        if (StringUtils.isBlank(position_name) == true) {
            messages.add("部署・役職名を入力してください");
        }
        //if (StringUtils.isBlank(password) == true) {
		//messages.add("パスワードを入力してください");
		//}
		if (!(StringUtils.isBlank(password)) & !(password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}+$"))) {
			messages.add("パスワードは半角英数記号で、6文字以上20文字以下に設定してください");
		}
		if(!(password.equals(password2))) {
			messages.add("パスワードが一致していません");
		}
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}