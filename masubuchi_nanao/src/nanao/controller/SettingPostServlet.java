		package nanao.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nanao.beans.UserMessage;
import nanao.service.UserMessageService;


@WebServlet(urlPatterns = { "/settingPost" })
public class SettingPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException {

		UserMessage dates = new UserMessage();

        String startdate = request.getParameter("startdate");
        String enddate = request.getParameter("enddate");

        dates.setStartdate(startdate);
        dates.setEnddate(enddate);

        request.setAttribute("dates", dates);

		UserMessage UserMessage = new UserMessage();
		UserMessage.setUser_name(request.getParameter("name"));
		UserMessage.setCategory(request.getParameter("category"));
		UserMessage.setSubject(request.getParameter("subject"));
		UserMessage.setText(request.getParameter("text"));

		List<UserMessage> messages = new UserMessageService().getMessage(UserMessage, startdate, enddate);

	    request.setAttribute("messages", messages);

	    request.getRequestDispatcher("settingPost.jsp").forward(request, response);
	}
}
