package nanao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import nanao.beans.NewMessage;
import nanao.beans.User;
import nanao.beans.UserMessage;
import nanao.service.NewMessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    		throws IOException, ServletException {

        request.getRequestDispatcher("newmessage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response)
           throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

    	UserMessage editMessage = getEditMessage(request);

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            String message = request.getParameter("text");
            String result = message.replace("&", "&amp;").replace("\"", "&quot;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&#39;");

            NewMessage newmessage = new NewMessage();
            newmessage.setSubject(request.getParameter("subject"));
            newmessage.setText(result);
            newmessage.setCategory(request.getParameter("category"));
            newmessage.setId(user.getId());

            session.setAttribute("newmessage", newmessage);

            new NewMessageService().register(newmessage);

            response.sendRedirect("./");
        } else {
            request.setAttribute("editMessage", editMessage);

        	NewMessage newmessage = new NewMessage();

        	String text = request.getParameter("text");
            String category = request.getParameter("category");
            String subject = request.getParameter("subject");

            newmessage.setSubject(subject);
            newmessage.setText(text);
            newmessage.setCategory(category);

            request.setAttribute("newmessage", newmessage);

            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("newmessage.jsp").forward(request, response);
        }
    }

	private UserMessage getEditMessage(HttpServletRequest request)
			throws IOException, ServletException{

		UserMessage editMessage =new UserMessage();
    	editMessage.setText(request.getParameter("text"));
    	editMessage.setCategory(request.getParameter("category"));
    	editMessage.setSubject(request.getParameter("subject"));
    	return editMessage;

	}

	private boolean isValid(HttpServletRequest request, List<String> emessages) {

        String message = request.getParameter("text");
        String category = request.getParameter("category");
        String subject = request.getParameter("subject");

        String result = message.replace("&", "&amp;").replace("\"", "&quot;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&#39;");

        if (StringUtils.isBlank(category) == true) {
            emessages.add("カテゴリーを入力してください");
        }
        if (!(StringUtils.isBlank(category)) & 10 < category.length()) {
            emessages.add("カテゴリーは10文字以下で入力してください");
        }
        if (StringUtils.isBlank(subject) == true) {
            emessages.add("件名を入力してください");
        }
        if (!(StringUtils.isBlank(subject)) & 30 < subject.length()) {
            emessages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isBlank(result) == true) {
            emessages.add("本文を入力してください");
        }
        if (!(StringUtils.isBlank(result)) & 1000 < result.length()) {
            emessages.add("本文は1000文字以下で入力してください");
        }
        if (emessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}