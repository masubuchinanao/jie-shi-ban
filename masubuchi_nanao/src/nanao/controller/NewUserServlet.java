package nanao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import nanao.beans.User;
import nanao.beans.branches;
import nanao.beans.positions;
import nanao.service.BranchService;
import nanao.service.PositionService;
import nanao.service.UserService;

@WebServlet(urlPatterns = { "/newuser" })
public class NewUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<branches> branches = new BranchService().getBranch();
    	request.setAttribute("branch", branches);

    	List<positions> positions = new PositionService().getPosition();
    	request.setAttribute("position", positions);

        request.getRequestDispatcher("newuser.jsp").forward(request, response);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User newUser = getUser(request);

        if (isValid(request, messages) == true) {

            User users = new User();
            users.setLogin_id(request.getParameter("login_id"));
            users.setPassword(request.getParameter("password"));
            users.setName(request.getParameter("name"));
            users.setBranch_id(Integer.parseInt(request.getParameter("branch_name")));
            users.setPosition_id(Integer.parseInt(request.getParameter("position_name")));

            new UserService().register(users);

            session.setAttribute("newUser", newUser);

            response.sendRedirect("userlist");

        } else {

        	request.setAttribute("editNewuser", newUser);

        	User newuser = new User();

        	String login_id = request.getParameter("login_id");
            String name = request.getParameter("name");

        	List<branches> branches = new BranchService().getBranch();
        	request.setAttribute("branch", branches);

        	List<positions> positions = new PositionService().getPosition();
        	request.setAttribute("position", positions);

        	newuser.setLogin_id(login_id);
        	newuser.setName(name);

        	newuser.setBranch_id(Integer.parseInt(request.getParameter("branch_name")));
        	newuser.setPosition_id(Integer.parseInt(request.getParameter("position_name")));

            request.setAttribute("newUser", newuser);

            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("newuser.jsp").forward(request, response);
        }
    }

    private User getUser(HttpServletRequest request)
    		throws IOException, ServletException {

    	User newUser = new User();
        newUser.setLogin_id(request.getParameter("login_id"));
        newUser.setPassword(request.getParameter("password"));
        newUser.setName(request.getParameter("name"));
        newUser.setBranch_id(Integer.parseInt(request.getParameter("branch_name")));
        newUser.setPosition_id(Integer.parseInt(request.getParameter("position_name")));
        return newUser;
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String branch_name = request.getParameter("branch_name");
        int branch_id = Integer.parseInt(request.getParameter("branch_name"));
        String position_name = request.getParameter("position_name");
        int position_id = Integer.parseInt(request.getParameter("position_name"));


        List<User> loginidList = new UserService().getLoginId(login_id);

        if (StringUtils.isBlank(name) == true) {
            messages.add("「名前」を入力してください");
        }
        if(!(StringUtils.isBlank(name) == true) & name.length() > 10) {
        	messages.add("「名前」は10文字以内で入力してください");
        }
        if(loginidList != null) {
        	messages.add("すでに使われている「ログインID」です");
        }
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("「ログインID」を入力してください");
        }
        if (!(StringUtils.isBlank(login_id)) & !(login_id.matches("^[0-9a-zA-Z]{6,20}+$"))) {
        	messages.add("「ログインID」は半角英数でかつ6文字以上20文字以下に設定してください");
        }
        if (StringUtils.isBlank(branch_name) == true) {
            messages.add("「支店名」を入力してください");
        }
        if (StringUtils.isBlank(position_name) == true) {
            messages.add("「部署・役職名」を入力してください");
        }
        if(branch_id == 17 && position_id > 3){
        	messages.add("本社の「部署・役職」を選択してください");
        }
        if(branch_id !=17 & position_id <= 3) {
        	messages.add("支店の「部署・役職」を選択してください");
        }
        if (StringUtils.isBlank(password) == true) {
            messages.add("「パスワード」を入力してください");
        }
        if (!(StringUtils.isBlank(password)) & !(password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}+$"))) {
        	messages.add("「パスワード」は半角英数記号でかつ6文字以上20文字以下に設定してください");
        }
		if (!(password.equals(password2))) {
			messages.add("「パスワード」が一致していません");
		}
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
}