package nanao.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nanao.beans.User;
import nanao.beans.UserComment;
import nanao.beans.UserMessage;
import nanao.service.CommentService;
import nanao.service.UserMessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User users = (User) request.getSession().getAttribute("loginUser");

        boolean newComment;
        if (users != null) {
        	newComment = true;
        } else {
        	newComment = false;
        }

        UserMessage dates = new UserMessage();

        String startdate = request.getParameter("startdate");
        String enddate = request.getParameter("enddate");
        String category = request.getParameter("category");

        dates.setCategory(category);
        dates.setStartdate(startdate);
        dates.setEnddate(enddate);

        List<UserMessage> messages = new UserMessageService().getMessage(startdate, enddate, category);
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("newComment", newComment);
        request.setAttribute("dates", dates);

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }
}