package nanao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import nanao.beans.Comment;
import nanao.beans.User;
import nanao.service.CommentService;


@WebServlet(urlPatterns = { "/newComment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    		throws IOException, ServletException {

        request.getRequestDispatcher("./").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
    		HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();

        if (isValid(request, comments) == true) {

            User user = (User) session.getAttribute("loginUser");

            String newcomment = request.getParameter("comment");
            String result = newcomment.replace("&", "&amp;").replace("\"", "&quot;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&#39;");

            Comment comment = new Comment();
            comment.setText(result);
            comment.setUserId(user.getId());
            comment.setMessageId(Integer.parseInt(request.getParameter("id")));

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> commenterror) {

        String comment = request.getParameter("comment");

        if (StringUtils.isBlank(comment) == true) {
        	commenterror.add("コメントを入力してください");
        }
        if (!(StringUtils.isBlank(comment)) & 500 < comment.length()) {
        	commenterror.add("500文字以下で入力してください");
        }
        if (commenterror.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}