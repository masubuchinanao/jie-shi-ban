package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import nanao.beans.UserMessage;
import nanao.dao.UserMessageDao;

public class UserMessageService {

	 public List<UserMessage> getMessage(String startdate, String enddate, String category) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserMessageDao UserMessageDao = new UserMessageDao();

	            Date date = new Date();
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	            String str = sdf.format(date);

	            String starttime;
	            String endtime;

	            if(StringUtils.isEmpty(startdate)) {
	            	starttime = "2019-01-01 00:00:00";
	            }else {
	            	starttime = startdate + " 00:00:00";
	            }

	            if(StringUtils.isEmpty(enddate)){
	            	endtime = str;
	            }else {
	            	endtime = enddate + " 23:59:59";
	            }

	            List<UserMessage> ret = UserMessageDao.getUserMessages(connection, starttime, endtime, category);

	            commit(connection);
	            return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

    public List<UserMessage> getMessage(UserMessage UserMessage, String startdate, String enddate) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao UserMessageDao = new UserMessageDao();

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String str = sdf.format(date);

            String starttime;
            String endtime;

            if(StringUtils.isEmpty(startdate)) {
            	starttime = "2019-01-01 00:00:00";
            }else {
            	starttime = startdate + " 00:00:00";
            }
            if(StringUtils.isEmpty(enddate)){
            	endtime = str;
            }else {
            	endtime = enddate + " 23:59:59";
            }

            List<UserMessage> ret = UserMessageDao.getUserMessages(connection, starttime, endtime, UserMessage);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
