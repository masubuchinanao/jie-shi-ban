package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;

import nanao.dao.DeleteCommentDao;

public class DeleteCommentService {

	public void register(int id) {
        Connection connection = null;
        try {
            connection = getConnection();

            DeleteCommentDao deleteCommentDao = new DeleteCommentDao();
            deleteCommentDao.delete(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
