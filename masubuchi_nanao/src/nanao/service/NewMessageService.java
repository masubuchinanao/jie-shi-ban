package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;

import nanao.beans.NewMessage;
import nanao.dao.NewMessageDao;

public class NewMessageService {

    public void register(NewMessage messages) {

        Connection connection = null;
        try {
            connection = getConnection();

            NewMessageDao messageDao = new NewMessageDao();
            messageDao.insert(connection, messages);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}