package nanao.service;


import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;

import nanao.beans.User;
import nanao.dao.AccountDao;

	public class AccountService {

		public User getAccount(int id, int working) {

		    Connection connection = null;
		    try {
				connection = getConnection();

				AccountDao accountDao = new AccountDao();
				User account = accountDao.getUser(connection, id, working);

				commit(connection);

		        return account;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}

	    public void update(int id,int working) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            AccountDao accountDao = new AccountDao();
	            accountDao.update(connection, id,working);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	}