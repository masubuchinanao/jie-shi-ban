package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;

import nanao.beans.User;
import nanao.dao.UserDao;
import nanao.utils.CipherUtil;

public class LoginService {

    public User login(String loginId, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User users = userDao.getUser(connection, loginId, encPassword);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}