package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;

import nanao.beans.User;
import nanao.dao.SettingDao;
import nanao.utils.CipherUtil;

public class SettingService {

	public User getUser(int id) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        SettingDao settingDao = new SettingDao();
	        User user = settingDao.getUser(connection, id);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

    public void update(User user, String password) {

        Connection connection = null;
        try {
            connection = getConnection();


            if(password.isEmpty() == false) {
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }

            SettingDao settingDao = new SettingDao();
            settingDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
