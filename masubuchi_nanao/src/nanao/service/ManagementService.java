package nanao.service;


import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import nanao.beans.User;
import nanao.dao.ManagementDao;

public class ManagementService {

	public List<User> getUser(User user) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        ManagementDao managementDao = new ManagementDao();
	        List<User> ret = managementDao.getUser(connection, user);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
