package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import nanao.beans.User;
import nanao.dao.UserDao;
import nanao.utils.CipherUtil;

public class UserService {

    public void register(User users) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(users.getPassword());
            users.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, users);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<User> getLoginId(String login_id) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<User> user = userDao.getUser(connection, login_id);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}