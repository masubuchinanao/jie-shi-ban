package nanao.service;

import static nanao.utils.CloseableUtil.*;
import static nanao.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import nanao.beans.positions;
import nanao.dao.PositionDao;

public class PositionService {

	public List<positions> getPosition() {
        Connection connection = null;
        try {
            connection = getConnection();

            PositionDao positionDao = new PositionDao();
            List<positions> positions = positionDao.getPosition(connection);

            commit(connection);

            return positions;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
